import {Injectable} from "@angular/core";
import {ProductModel} from "./product.model";
import {from, Observable} from "rxjs";


@Injectable()
export class StaticDataSource {
  private products: ProductModel[] = [
    new ProductModel(1, "Product 1", "Category 1", "Product 1 (Category 1)", 100),
    new ProductModel(2, "Product 2", "Category 1", "Product 2 (Category 1)", 100),
    new ProductModel(3, "Product 3", "Category 1", "Product 3 (Category 1)", 100),
    new ProductModel(4, "Product 4", "Category 1", "Product 4 (Category 1)", 100),
    new ProductModel(5, "Product 5", "Category 1", "Product 5 (Category 1)", 100),
    new ProductModel(6, "Product 6", "Category 2", "Product 6 (Category 2)", 100),
    new ProductModel(7, "Product 7", "Category 2", "Product 7 (Category 2)", 100),
    new ProductModel(8, "Product 8", "Category 2", "Product 8 (Category 2)", 100),
    new ProductModel(9, "Product 9", "Category 2", "Product 9 (Category 2)", 100),
    new ProductModel(10, "Product 10", "Category 2", "Product 10 (Category 2)", 100),
    new ProductModel(11, "Product 11", "Category 3", "Product 11 (Category 3)", 100),
    new ProductModel(12, "Product 12", "Category 3", "Product 12 (Category 3)", 100),
    new ProductModel(13, "Product 13", "Category 3", "Product 13 (Category 3)", 100),
    new ProductModel(14, "Product 14", "Category 3", "Product 14 (Category 3)", 100),
    new ProductModel(15, "Product 15", "Category 3", "Product 15 (Category 3)", 100),
  ];

  getProducts(): Observable<ProductModel[]> {
    return from([this.products]);
  }
}
