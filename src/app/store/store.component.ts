import { Component, OnInit } from '@angular/core';
import {ProductRepository} from "../model/product.repository";
import {ProductModel} from "../model/product.model";

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.sass'],
})
export class StoreComponent implements OnInit {

  constructor(private repository: ProductRepository) { }

  ngOnInit(): void {
  }

  get products(): ProductModel[] {
    return this.repository.getProducts();
  }

  get categories(): string[] {
    return this.repository.getCategories();
  }

}
